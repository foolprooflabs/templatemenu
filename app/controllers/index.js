function doClick(e) {
    alert($.label.text);
}

// Action Bar - FAKE example
var actionBar = Ti.UI.createView({
	top:0,
	height:"44dp",
	backgroundColor:"#FFDE00"
});
var leftToolbarBtn = Ti.UI.createImageView({
	image:"/appicon.png",
	left: "6dp",
	width: Ti.UI.SIZE,
	backgroundColor:"transparent",
	color: "#FFF"
});

var rightToolbarBtn = Ti.UI.createImageView({
	image:"/customoverflow.png",
	right: "6dp",
	backgroundColor:"transparent",
});
rightToolbarBtn.addEventListener("click", function(){
	$.drawer.toggleRightWindow();
});
var centerLabel = Ti.UI.createImageView({
	image:"/actionbar_background.png",
	font:{
		fontSize:"14dp",
		fontWeight:"bold"
	},
});
actionBar.add(leftToolbarBtn);
actionBar.add(rightToolbarBtn);
actionBar.add(centerLabel);
$.centerView.add(actionBar);
$.drawer.open();